# Google Recaptcha implementation

This code is to install the Google Recaptcha validator in a form. It's simple to use:

- Go to [Google Recaptcha](https://www.google.com/recaptcha/) and create a new captcha. Get the secret.
- Add the class RecaptchaResponse.class.php in your library folder of your proyect.
- Add the content of the form of the form.html file into your form at the end.
- Validate the form like in checkTheForm.php file.

Easy!