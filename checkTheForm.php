<?php
require_once dirname(__FILE__) . '/recaptchalib.php';
$secret = "6LdlQGYUAAAAACe71KBEiax542s5QDb0iognkCcF"; // Change for yours
$response = null;
// comprueba la clave secreta
$reCaptcha = new ReCaptcha($secret);

if ($_POST["g-recaptcha-response"]) {
   $response = $reCaptcha->verifyResponse(
   $_SERVER["REMOTE_ADDR"],
   $_POST["g-recaptcha-response"]
   );
}

if ($response != null && $response->success) {
   // Do the magic
} else {
   // Bad response
}